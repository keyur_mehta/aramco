aramcoApp.factory('EventService', function ($rootScope,$http,$route) {
        var service = {};

        service.login = function (username, password, callback, errorcallback) {

		   $http.get(service_url + 'Login/CheckLogin?userName='+username+'&password='+password ,  {timeout: timeout_period})
		   .success(function (data) {
			   callback(data);
		   })
		   .error(function (data){
			   errorcallback();
		   });
        };
        
        service.getEvents = function (callback,errorcallback) {
            $http.get(service_url + 'Event?userName=' + $rootScope.$storage.username,  {timeout: timeout_period})
              .success(function (response) {
                   callback(response);
              })
			  .error(function (response){
			   		errorcallback();
		      });
         };
         
        service.getEventData = function (eventId,callback , errorcallback) {
            $http.get(service_url + 'Event/DownloadEventServiceModels/'+eventId, {timeout: timeout_period})
               .success(function (response) {
                   callback(response);
               })
               .error(function (response){
				   errorcallback();
			   });
         };
	
	 	service.syncDataToServer = function (eventId,candidateList,callback,errorcallback) {
			 var request = $http({
			 method: "post",
			 url: service_url + "SyncCandidate?eventId="+eventId,
			 data: candidateList,
			 headers: { 'Content-Type': 'application/json' }
			 });
			 request.success(function (data) {
				callback(data);
			 });
			 request.error(function (data) {
				errorcallback();
			 });
         };
	
        return service;
    });
 

