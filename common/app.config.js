aramcoApp.config(function($routeProvider) {
        $routeProvider
            .when('/login', {
                templateUrl : 'views/login.html',
                controller  : 'login',
            })
		 	.when('/home', {
                templateUrl : 'views/home.html',
                controller  : 'home',
            })
            .when('/recruitersarea', {
                templateUrl : 'views/recruitersarea.html',
                controller  : 'recruitersarea',
            })
            .when('/events', {
                templateUrl : 'views/events.html',
                controller  : 'events',
            })
            .when('/home-eventmode', {
                templateUrl : 'views/home-eventmode.html',
                controller  : 'homeeventmode',
            })
            .when('/jointc/:id', {
                templateUrl : 'views/jointc.html',
                controller  : 'jointc',
            })
            .when('/registration/:status', {
                templateUrl : 'views/registration.html',
                controller  : 'registration',
            })
            .when('/candidate/:id', {
                templateUrl : 'views/candidatedetail.html',
                controller  : 'candidatedetail',
            })
            .otherwise({
            	templateUrl : 'views/login.html',
                controller  : 'login',
      	    });           
});


