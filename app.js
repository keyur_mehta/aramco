//document.addEventListener("deviceready",startup);
startup();

/* db creation */
function startup() {
    console.log("Starting up...");
    db = window.openDatabase(db_name,db_version,db_description,db_memory);
    db.transaction(init_dB,db_error,db_ready);
}


function init_dB(tx) {
	
	
	tx.executeSql("CREATE TABLE if not exists candidates(id INTEGER PRIMARY KEY AUTOINCREMENT, candidate_id INTEGER, candidate_inviteecode TEXT, candidate_firstname TEXT, candidate_lastname TEXT,candidate_email TEXT,candidate_area_interest INTEGER ,candidate_employername TEXT , candidate_jobtitle TEXT, candidate_mobileno TEXT ,candidate_invitee_statusid  INTEGER , candidate_joincommunity  TEXT ,candidate_profilesummary  TEXT ,candidate_dirty INTEGER ,candidate_synced INTEGER , last_updated TEXT , error_message TEXT)",
	[],
	function (tx, result) {
		console.log("Table Successfully Created");
	},
	function (tx, error) {
		console.log("Query Error: " + error);
	}
	);
}

function db_ready() {
    console.log("DB initialized successfully.");
}

function db_error(error) {
     console.log("DB Error" + error);
}

/* db creation */

/* Current Date Time */
function system_datetime(){
	var currentdate = new Date(); 
	var datetime =  currentdate.getDate()  + "-" 
                + (currentdate.getMonth()+1)  + "-"
                + currentdate.getFullYear() + " "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
	
	return datetime;
}
/* Current Date Time */


function goback(){
	window.history.back();
}

aramcoApp.run(function ($rootScope,$localStorage,$http,$route,$location , $window) 
{
	$rootScope.event_exists = false;
	
    $rootScope.$storage = $localStorage.$default({
		pin			    : 1234,
		rememberlogin   : false,
		username 	    : null,
		current_event   : null,
		areas_interest  : null,
		invitee_status  : null,
        profiledata     : null,
        masterpin		: 'aramco$123#'
	});
	
	$rootScope.$storage.masterpin = 'aramco$123#';
	$rootScope.setTitle = function(title,middlecontent) {
		document.getElementById('header-title').innerHTML = title;	
		$rootScope.headermode = middlecontent;
        if($rootScope.$storage.current_event){
              $rootScope.event_exists = true;
        }
	};
	
	$rootScope.setSubTitle = function(subtitle,showsubheader) {
		document.getElementById('sub-title').innerHTML = subtitle;	
		$rootScope.subheader = showsubheader;
	};
	
	$rootScope.changemode = function(){
		if($rootScope.headermode == 'event'){
			 $location.path('/home-eventmode');
		}else{
			 $location.path('/home');		
		}
	};
	
	$rootScope.resetbackground = function(){
		document.getElementById("app").style.opacity = "1.0";
	};
	  
    /* Check Internet Connection */
    $rootScope.checkConnection = function() {
        /*
        if(navigator.network.connection.type == Connection.NONE)
        {
               navigator.notification.alert('Please check your Internet Connection', function(){}, "Error", "");
               return false;
        }
        else
        {
               return true;
        }
        */
         return true;
    };
	
});
       




