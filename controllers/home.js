aramcoApp.controller('home', function($scope,$rootScope, $location, $uibModal, $log,EventService) {	
	
	$rootScope.setTitle('<i class="fa fa-home"></i>&nbsp;Login ','setup');
	$rootScope.setSubTitle('',false);
	
	$scope.formInfo = {};
	$scope.formInfo.remember 		=  $rootScope.$storage.rememberlogin;
	
	if($rootScope.$storage.rememberlogin){
		$scope.formInfo.username   		=  $rootScope.$storage.username;
	}else{
		$scope.formInfo.username 		=  null;
	}
	
	$scope.login = function () {
		if($rootScope.checkConnection()){
			$scope.validlogin()
		}
     };
	
	$scope.validlogin = function () {
            $scope.viewLoading = true;
            EventService.login($scope.formInfo.username, $scope.formInfo.password, function(data) {
				$scope.viewLoading = false;
                if(data.Response) {
					if($scope.formInfo.remember){
						$rootScope.$storage.rememberlogin = true;
					}else{
						$rootScope.$storage.rememberlogin = false;
					}
					$rootScope.$storage.username 	  = $scope.formInfo.username;
                    $location.path('/recruitersarea');
                }else{
                    navigator.notification.alert('Incorrect Username or Password', function(){}, "Error", "");
                }
				
            },function(){
				$scope.viewLoading = false;
				navigator.notification.alert('Something Went Wrong. Please Try again', function(){}, "Error", "");
			}
			);
     };
     
    
    $scope.open = function () {
		document.getElementById("app").style.opacity = "0.5";
		var modalInstance = $uibModal.open({
		  animation: true,
		  templateUrl: 'changepin.html',
		  controller: 'changepin'
		});
	 };
 
});



