aramcoApp.controller('candidatedetail', function ($scope, $rootScope, $location, $routeParams , $route) {
	$rootScope.setTitle('<i class="fa fa-users"></i>&nbsp;Registration','');
	$rootScope.setSubTitle('<a href="#/registration/all"><i class ="fa fa-arrow-circle-o-left"></a>',true);
    
    var id = $routeParams.id;
    $scope.viewLoading = true;
    $scope.candidateinfo = {};
    $scope.candidateinfo.candidate_interest = null;
    
    db.transaction(function (tx)
	{
		var query =  'SELECT * FROM candidates where id = "'+id+'"';
		tx.executeSql(query, [], function (tx, results)
		{
			 $scope.candidateinfo = results.rows.item(0)
			 for(index = 0; index < $rootScope.$storage.areas_interest.length ; index++){
				 if($rootScope.$storage.areas_interest[index].AreasOfInterestId == $scope.candidateinfo.candidate_area_interest){
					 $scope.candidateinfo.candidate_interest = $rootScope.$storage.areas_interest[index].AreasOfInterestDescription;
				 }
			 }
			 
			 $scope.$apply(function(){
					$scope.viewLoading = false;
			});
		}, null); 
	});
	
	$scope.attendance = function(){
		 navigator.notification.confirm(
        'Confirm '+  $scope.candidateinfo.candidate_firstname + ' ' +  $scope.candidateinfo.candidate_lastname + ' Attendance at Todays Event ? ',
            function(button){
                if(button == 1){
                    var datetime = system_datetime();
					db.transaction(function (tx) 
					{	
						tx.executeSql('UPDATE candidates set candidate_invitee_statusid = 6,candidate_dirty = 1 , last_updated = ? where id = ?',[datetime, $scope.candidateinfo.id]);
						$route.reload();
					});
					navigator.notification.alert('Successfully Changed', function(){}, "Success", "");
                }
            },
        'Confirm Attendance ?',
        ['Yes','No']
		);
	}
   
});
