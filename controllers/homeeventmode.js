aramcoApp.controller('homeeventmode', function ($scope,$rootScope,$cordovaOauth , $http , $location ) {
    $rootScope.setTitle('<i class="fa fa-home"></i>&nbsp;Home','event');
	$rootScope.setSubTitle('',false);
    $scope.viewLoading = false ;
    
    /* Open Linkedin*/
    $scope.openlinkedin = function (){
        if($rootScope.checkConnection()){
			$scope.loginlinkedin();
		}
    };
    
    $scope.loginlinkedin = function() {
        
         $rootScope.$storage.profiledata = [];
         $cordovaOauth.linkedin("75heu1dxve9pqd","1yELSRJmh8K7nYme",["r_emailaddress"] ,"DCEeFWf45A53sdfKef424" ).then(function(result) {
                 $scope.viewLoading = true ;
                 $http.get("https://api.linkedin.com/v1/people/~:(first-name,last-name,positions,email-address,summary)?oauth2_access_token="+result.access_token+"&format=json")
                 .then(function(response) {
                 
                    $rootScope.$storage.profiledata['emailaddress'] = response.data.emailAddress;
                    $rootScope.$storage.profiledata['firstname']    = response.data.firstName;
                    $rootScope.$storage.profiledata['lastname']     = response.data.lastName;
                    $rootScope.$storage.profiledata['jobtitle']     = response.data.positions.values[0].title;
                    $rootScope.$storage.profiledata['employer']     = response.data.positions.values[0].company.name;
                    
                    $location.path('/jointc/-999');
                    
                 }, function(error) {
                    $scope.viewLoading = false ;
                    alert("There was a problem getting the profile data. Please try again.");
                });
                 
        }, function(error) {
           console.log(error);
        });
    };
    
});
