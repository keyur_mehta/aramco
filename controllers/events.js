aramcoApp.controller('events', function ($scope, $rootScope, EventService, $http, $uibModal) {
	$rootScope.setTitle('<i class="fa fa-calendar-o"></i>&nbsp;View Events','');
	$rootScope.setSubTitle('<a href="#/recruitersarea"><i class ="fa fa-arrow-circle-o-left"></i></a>',true);
	
	$scope.events = [];
	$scope.viewLoading = true;
	
	/* Get all events */
	EventService.getEvents(function (response) {
			if(response) {
				$scope.events = response;
				$scope.viewLoading = false;
			}
    },function(){
			$scope.viewLoading = false;
			navigator.notification.alert('Something Went Wrong. Please Try again', function(){}, "Error", "");
		
	}
	);
    
    /* open  caution modal box */
    $scope.download = function(event){	
		document.getElementById("app").style.opacity = "0.5";
		var modalInstance = $uibModal.open({
		  animation: true,
		  templateUrl: 'caution.html',
		  controller: 'caution',
		   resolve: {
				eventDetail: function () {
				  return event;
		        }
		  }
		});
	};
    
});

aramcoApp.controller('caution', function ($scope, $http, $uibModalInstance, $uibModal, eventDetail,$rootScope) {

	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
		$rootScope.resetbackground();
	};
	
	/* open device pin modal box */
	$scope.reset = function () {
		$uibModalInstance.dismiss('cancel');
		var modalInstance = $uibModal.open({
		  animation: true,
		  templateUrl: 'devicepin.html',
		  controller: 'devicepin',
		  resolve: {
			newEvent: function () {
			  return eventDetail;
			}
		  }
		});
	};
	
});

aramcoApp.controller('devicepin', function ($scope, $http, $uibModalInstance, $rootScope, EventService, newEvent) {
	$scope.dataLoading = false;
	var candidates = null;
	$scope.application_pin = null;
	
	$scope.cancel = function () {
		$uibModalInstance.dismiss('cancel');
		$rootScope.resetbackground();
	};
	
	$scope.downloadevent = function(){
		if($rootScope.checkConnection()){
			$scope.reset()
		}
	}
	
	$scope.reset = function () {
		$scope.dataLoading = true;
		if($rootScope.$storage.pin != $scope.application_pin){
           navigator.notification.alert('Incorrect Application Pin. Please Try Again', function(){}, "Error", "");
			$scope.application_pin = null;
			$scope.dataLoading = false;
		}else{
			
			/* Get Event Data */
			
			EventService.getEventData(newEvent.EventId, function(response) {
				
				$rootScope.$storage.current_event = {
				 'event_id' 		: newEvent.EventId,
				 'event_name' 		: newEvent.EventName,
				 'event_desc' 		: newEvent.EventDescription ,
				 'event_type' 		: newEvent.EventType ,
				 'event_startdate'  : newEvent.EventStartDate ,
				 'event_enddate' 	: newEvent.EventEndDate ,
				 'event_status'		: newEvent.EventStatus,
				 'event_location' 	: newEvent.EventLocation
				}; 
				
				$rootScope.$storage.areas_interest = response.ReferenceData.AreasofInterestSericeModels;
				$rootScope.$storage.invitee_status = response.ReferenceData.InviteeStatuses;
				
				candidates = response.Candidates;
				var datetime = system_datetime();
				db.transaction(function (tx)
				{
					tx.executeSql('DELETE FROM candidates', []);
					for(index = 0;index < candidates.length ; index++)
					{
						var data = [candidates[index].CandidateId,candidates[index].CandidateInviteeCode,candidates[index].FirstName,candidates[index].LastName,candidates[index].Email,candidates[index].AreasOfInterestId,candidates[index].CurrentEmployerName,candidates[index].CurrentJobTitle,candidates[index].MobileNo,candidates[index].InviteeStatusId,candidates[index].JoinCommunityForEventFlage,candidates[index].ProfileSummary,0,1,datetime , null];
					
						tx.executeSql("INSERT INTO candidates(candidate_id, candidate_inviteecode, candidate_firstname, candidate_lastname,candidate_email,candidate_area_interest,candidate_employername,candidate_jobtitle,candidate_mobileno,candidate_invitee_statusid,candidate_joincommunity,candidate_profilesummary,candidate_dirty,candidate_synced,last_updated , error_message) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?)", 
							[
								data[0], data[1], data[2], data[3], data[4] ,data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]
							] ,
							function (tx, result) {
								console.log("Successfully Inserted : " + index);
							},
							function (tx, error) {
								console.log("Query Error: " + error.message + index);
							}
						); 
					}
				});
				
				$scope.dataLoading = false;
				
				$uibModalInstance.dismiss('cancel');
				$rootScope.resetbackground();
                 navigator.notification.alert('Successfully Downloaded', function(){}, "Success", "");
			},function(){
				$scope.dataLoading = false;
				navigator.notification.alert('Something Went Wrong. Please Try again', function(){}, "Error", "");
			}
			
			);
		}
	};
	
});




