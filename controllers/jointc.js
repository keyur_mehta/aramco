aramcoApp.controller('jointc', function ($scope, $rootScope, $location, $routeParams) {
	$rootScope.setTitle('<i class="fa fa-users"></i>&nbsp;Registration','');
	$rootScope.setSubTitle('<a onclick="goback()"><i class ="fa fa-home"></a>',true);
    
    var id = $routeParams.id;
    
    if (id == 0) {
        $scope.type = 'addForm';
    }else if (id == -999) {
        $scope.type = 'linkedinForm';
    }
	else {
        $scope.type = 'editForm';
    }
    
    $scope.formInfo = {};
    
    
    if ($scope.type == 'addForm') {
       
        $scope.formInfo.current_employer = null;
        $scope.formInfo.job_title        = null;
        $scope.formInfo.mobile_number    = null;
        $scope.formInfo.profile_summary  = null;
        $scope.additionalinfo = false;
		$scope.formInfo.event_join = true;
        $scope.formInfo.area_interest = '';
        
    }else if ($scope.type == 'linkedinForm') {
		
        $scope.formInfo.current_employer = $rootScope.$storage.profiledata['employer'];
        $scope.formInfo.job_title        = $rootScope.$storage.profiledata['jobtitle'];
        $scope.formInfo.first_name       = $rootScope.$storage.profiledata['firstname'];
        $scope.formInfo.last_name  		 = $rootScope.$storage.profiledata['lastname'];
		$scope.formInfo.email_address 	 = $rootScope.$storage.profiledata['emailaddress'];
		$scope.formInfo.area_interest = '';
        $scope.additionalinfo 			 = true;
		$scope.formInfo.event_join       = true;
		
		
    } else {
        
        $scope.additionalinfo = true;
        
        db.transaction(function (tx) {
            var query =  'SELECT * FROM candidates where id = "' + id + '"';               
            tx.executeSql(query, [], function (tx, results) { 
                for (var i=0; i<results.rows.length;i++)
                {
					
                    $scope.$apply(function() {
                        $scope.formInfo.first_name      =   results.rows.item(i).candidate_firstname;
                        $scope.formInfo.last_name        =  results.rows.item(i).candidate_lastname;
                        $scope.formInfo.email_address    =  results.rows.item(i).candidate_email;
                        $scope.formInfo.area_interest    =  results.rows.item(i).candidate_area_interest;
                        $scope.formInfo.current_employer =  results.rows.item(i).candidate_employername;
                        $scope.formInfo.job_title        =  results.rows.item(i).candidate_jobtitle;
                        $scope.formInfo.mobile_number    =  results.rows.item(i).candidate_mobileno;
                        $scope.formInfo.profile_summary  =  results.rows.item(i).candidate_profilesummary;
                        $scope.formInfo.event_join       =  results.rows.item(i).candidate_joincommunity;
                    });
                }
                
            }, null); 
          
        });
    }
    
    $scope.showadditional = function () {
		$scope.additionalinfo = true;
	};
   
    $scope.join = function () {
		var datetime = system_datetime();
        db.transaction(function (tx)
        {
            if (id == 0 || id == -999){
				
                var data = [ 0,'',$scope.formInfo.first_name,$scope.formInfo.last_name,$scope.formInfo.email_address,$scope.formInfo.area_interest,$scope.formInfo.current_employer,$scope.formInfo.job_title,$scope.formInfo.mobile_number,'1',$scope.formInfo.event_join,$scope.formInfo.profile_summary,1,0,datetime,null];
                

                tx.executeSql("INSERT INTO candidates(candidate_id, candidate_inviteecode, candidate_firstname, candidate_lastname,candidate_email,candidate_area_interest,candidate_employername,candidate_jobtitle,candidate_mobileno,candidate_invitee_statusid,candidate_joincommunity,candidate_profilesummary,candidate_dirty,candidate_synced,last_updated , error_message) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
                    [
                        data[0], data[1], data[2], data[3], data[4] ,data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14] , data[15]
                    ],
                    function (tx, result) {
                        console.log("Successfully Inserted : ");
                    },
                    function (tx, error) {
                        console.log("Query Error: " + error.message);
                    }
                ); 
            } else {
               
                  var data = [$scope.formInfo.first_name,$scope.formInfo.last_name,$scope.formInfo.email_address,$scope.formInfo.area_interest,$scope.formInfo.current_employer,$scope.formInfo.job_title,$scope.formInfo.mobile_number,$scope.formInfo.event_join,$scope.formInfo.profile_summary,1 , datetime];
            
                tx.executeSql('UPDATE candidates set candidate_firstname = ? , candidate_lastname = ? ,candidate_email = ? , candidate_area_interest = ? ,candidate_employername = ? ,candidate_jobtitle = ? ,candidate_mobileno = ? ,candidate_joincommunity = ? , candidate_profilesummary = ? , candidate_dirty = ? , last_updated = ?  where id = ?',
                [
                    data[0],data[1],data[2],data[3] ,data[4], data[5] , data[6] , data[7] , data[8] , data[9] , data[10] , id
                ],
                function (tx, result) {
                        console.log("Successfully Updated : ");
                },
                function (tx, error) {
                        console.log("Query Error: " + error.message);
                }
                             
                );
                
            }
            
        });
        
        navigator.notification.alert('Successfully Submitted', function(){}, "Success", "");
        $location.path('/registration/all');
	};
    
});
