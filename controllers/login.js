aramcoApp.controller('login', function($scope,$rootScope,$location , $uibModal) 
{	
    $scope.mode = 'Setup';
    $scope.current_event = false;
	
	$rootScope.setTitle('<i class="fa fa-lock"></i>&nbsp;Unlock ','');
    
    if($rootScope.$storage.current_event){
        $scope.current_event = true;
    }
   
	$scope.checkpin = function () {
		if($scope.formInfo.pin != $rootScope.$storage.pin){
            navigator.notification.alert('Incorrect Pin. Please Try Again', function(){}, "Error", "");
		}else{
			$scope.error = false;
            if($scope.mode == 'Event'){
                $location.path('/home-eventmode');
            }else{
                $location.path('/home');
            }
			
		}
	};
	
	$scope.openmasterpin = function(){
		document.getElementById("app").style.opacity = "0.5";
		var modalInstance = $uibModal.open({
		  animation: true,
		  templateUrl: 'masterpin.html',
		  controller: 'masterpin'
		});
	};
	
});
