aramcoApp.controller('registration', function($scope,$rootScope,$location,$route , $routeParams , $window) 
{	
	$rootScope.setTitle('<i class="fa fa-users"></i>&nbsp;&nbsp;Registration','');
	var where = " ";
	var status = $routeParams.status;
	$scope.showerrormessage = false;
	
	if (status == 'pending') {
		where += 'and candidate_dirty = 1';
		$scope.showerrormessage = true;
		$rootScope.setSubTitle('<a onclick="goback()" class="registration"><i class ="fa fa-arrow-circle-o-left"></i></a>',true);
	}else{
		$rootScope.setSubTitle('<a href="#/home-eventmode" class="registration"><i class ="fa fa-arrow-circle-o-left"></i></a>',true);
	}
    
    $scope.searchemail = null;
    $scope.candidates  =[];
    $scope.searched    = false;
   
    
    /* Search Candidates By Email */
    $scope.search = function () {
        $scope.viewLoading = true;
        $scope.candidates =[];
        $scope.searched    = true;
        
        db.transaction(function (tx)
        {
            if($scope.searchemail){
              var query =  'SELECT * FROM candidates where candidate_email LIKE "%'+$scope.searchemail+'%" ' + where +' order by id desc';
            }else{
               var query =  'SELECT * FROM candidates where 1 = 1 ' + where +' order by id desc';
            }
			
            tx.executeSql(query, [], function (tx, results)
            {
                for(var i=0; i<results.rows.length;i++)
                {
                    $scope.$apply(function(){
                        $scope.candidates.push(results.rows.item(i));
                    });
                }
                 console.log($scope.candidates);
                 $scope.$apply(function(){
                        $scope.viewLoading = false;
                });
            }, null); 
          
        });
    };
    /* Search Candidates By Email */
    
    $scope.search();
    
    /* Attendance -> Set Attended , Dirty Flag , and not synced */
    $scope.attendance = function (candidateid,candidatefirstname,candidatelastname) {
		 navigator.notification.confirm(
        'Confirm '+ candidatefirstname + ' ' + candidatelastname + ' Attendance at Todays Event ? ',
            function(button){
                if(button == 1){
                    var datetime = system_datetime();
					db.transaction(function (tx) 
					{	
						tx.executeSql('UPDATE candidates set candidate_invitee_statusid = 6,candidate_dirty = 1 , last_updated = ? where id = ?',[datetime, candidateid]);
						$route.reload();
					});
					navigator.notification.alert('Successfully Changed', function(){}, "Success", "");
                }
            },
        'Confirm Attendance ?',
        ['Yes','No']
		);
	 
    };
    /* Attendance */
    
    /* Delete unsynced Candidate */
     $scope.delete = function (candidateid) {
                     
    navigator.notification.confirm(
        'Do You want to delete this candidate data from the Event ?',
            function(button){
                if(button == 1){
                    db.transaction(function (tx)
                    {
                        tx.executeSql('DELETE from candidates where id = ?',[candidateid]);
                    });
                    navigator.notification.alert('Successfully Deleted', function(){}, "Success", "");
                    $route.reload();
                }
            },
        'Delete Candidate ?',
        ['Yes','No']
    );
        
    };
    /* Delete unsynced Candidate */
    
});
