aramcoApp.controller('recruitersarea', function ($scope, $rootScope, $http, $location, EventService , $uibModal , $route ) {
	
	$rootScope.setTitle('<span class="recruiterarea"><i class="fa fa-search-plus"></i>&nbsp;Recruiter\'s Area </span>', '');
	$rootScope.setSubTitle('<a href="#/home"><i class ="fa fa-power-off"></a>', true);
	
	var candidateList       = [];
	var candidatesuccessList= [];
	var candidateerrorList  = [];
	var totaldatasynced 	= 0;
	var lastid 				= 0;
	var syncpending 		= 0;
	
	$scope.total 			= 0;
	$scope.pending			= 0;
	$scope.viewLoading 		= false;
	$scope.syncerror 		= 0;
	
	db.transaction(function (tx)
    {
		/* total candidates */
		tx.executeSql('SELECT * FROM candidates', [], function (tx, results)
		{
			console.log(results.rows.length);
			  $scope.$apply(function() {
				$scope.total = results.rows.length;
			  });
		}, null); 
		
		/* Candidates left to sync */
		tx.executeSql('SELECT * FROM candidates where candidate_dirty = 1', [], function (tx, results)
		{
			console.log(results.rows.length);
			  $scope.$apply(function() {
				$scope.pending = results.rows.length;
				syncpending = $scope.pending;
			  });
		}, null); 
		
	});
	
	$scope.downloadevent = function(){
		if($rootScope.checkConnection()){
			$location.path('/events');
		}
	};
	
	
	$scope.syncdata = function(){
		if($rootScope.$storage.current_event){
			if(syncpending){
				if($rootScope.checkConnection()){
					$scope.viewLoading = true;
					/* start from first candidate which is not synced */
					$scope.syncerror 		= 0;
					$scope.getData(0);
				}
			}else{
                navigator.notification.alert('Data is already Synced', function(){}, "Warning", "");
			}	
		}else{
            navigator.notification.alert('No Event Found', function(){}, "Warning", "");
		}
	};
	
	$scope.getData = function (id){
		candidateList = [];
		candidatesuccessList = [];
		candidateerrorList = [];
		db.transaction(function (tx)
    	{
			if(id > 0){
				var concate = 'id > ' + id; 
			}else{
				var concate = '1 = 1';
			}
			
			tx.executeSql('SELECT * FROM candidates where candidate_dirty = 1 and ' + concate + ' LIMIT ' + sync_limit , [], function (tx, results)
			{
				for(var i=0; i<results.rows.length;i++)
				{
					var row = results.rows.item(i);
					if(typeof candidate_joincommunity === 'undefined'){
						row['candidate_joincommunity'] = false;
					}
					var candidateobject = 
					{
						EventAppCandidateReference : row['id'],
						CandidateId: row['candidate_id'],
						CandidateInviteeCode: row['candidate_inviteecode'],
						FirstName: row['candidate_firstname'],
						LastName: row['candidate_lastname'],
						Email: row['candidate_email'],
						AreasOfInterestId: row['candidate_area_interest'],
						CurrentEmployerName: row['candidate_employername'],
						CurrentJobTitle: row['candidate_jobtitle'],
						MobileNo: row['candidate_mobileno'],
						InviteeStatusId: row['candidate_invitee_statusid'],
						JoinCommunityForEventFlag: true,
						ProfileSummary: row['candidate_profilesummary'],
						LastUpdatedDate: row['last_updated']
				     };
					
					candidateList.push(candidateobject);
					console.log(candidateList);
					lastid = row['id'];
				}
				$scope.senddatatoserver();	
			}, null); 
			
		});
		
	}
	
	$scope.senddatatoserver = function(){
		
		 var candidatelength = candidateList.length;
		 var syncbatchcount = 0;
		 
		 EventService.syncDataToServer( $rootScope.$storage.current_event.event_id, candidateList, function(response) {
			  console.log(response);
			 if(response.RequestErrorMessage != null){
				navigator.notification.alert(response.RequestErrorMessage, function(){}, "Error", "");
				$scope.viewLoading = false;
			 }else{
				for (var index = 0; index < candidateList.length ; index++){
					 var candidatereference = response.SyncStatus[index].EventAppCandidateReference;
					 var error_message = response.SyncStatus[index].ErrorMessage;
					 /* if Response true push candidateid else update error message in db for the corresponding Candidate */
					 if(response.SyncStatus[index].Success){
						 var servercandidateid = response.SyncStatus[index].CandidateId;
						 syncbatchcount = syncbatchcount + 1;
						 var obj = {id :candidatereference , candidate_id : servercandidateid };
						 candidatesuccessList.push(obj);
					 }else{
						 $scope.syncerror  = $scope.syncerror + 1;
						 var obj = {errormessage :error_message , candidate_id : candidatereference };
						 candidateerrorList.push(obj);
					 }
				}
			
			   totaldatasynced = parseInt(totaldatasynced) + parseInt(candidatelength);
			   $scope.pending =  $scope.pending - syncbatchcount;
			   $scope.updateDirtyFlag();
		     }
		 },
		 function() {
			 $scope.viewLoading = false;
			 navigator.notification.alert('There occured some problem . Please Try again', function(){}, "Error", "");
			 $route.reload();
	     }			 
		 );
	}
	
	
	$scope.updateDirtyFlag = function(){
		db.transaction(function (tx)
		{
			/* update successful candidates */
			for (var i = 0; i< candidatesuccessList.length ; i++){
				console.log(candidatesuccessList[i].candidate_id );
				tx.executeSql('UPDATE candidates set candidate_dirty = 0 , candidate_synced = 1, candidate_id = ? where id = ?',
					[
						candidatesuccessList[i].candidate_id , candidatesuccessList[i].id
					],
					function (tx, result) {
						console.log("Successfully Updated : ");
					},
					function (tx, error) {
						console.log("Query Error: ");
					}       
				);
			}
			
			/* update unsuccessful candidates */
			for (var i = 0; i< candidateerrorList.length ; i++){
				tx.executeSql('UPDATE candidates set candidate_dirty = 1 , candidate_synced = 0 , error_message = ? where id = ?',
					[
						candidateerrorList[i].errormessage , candidateerrorList[i].candidate_id
					],
					function (tx, result) {
						console.log("Successfully Updated : ");
					},
					function (tx, error) {
						console.log("Query Error: ");
					}       
				);
			}
			
			if(totaldatasynced < syncpending){
				if(navigator.network.connection.type == Connection.NONE)
				{	
					navigator.notification.alert('Please check your Internet Connection', function(){}, "Error", "");
					$route.reload();
				}
				else
				{
					$scope.getData(lastid);
				}
			}else{
				$scope.$apply(function() {
					$scope.viewLoading = false;
				});
                navigator.notification.alert('Successfully Synced', function(){}, "Success", "");
			}
		});
	};
	
	
	/* open  caution modal box */
    $scope.redownload = function(){
	  if($rootScope.$storage.current_event){
		   document.getElementById("app").style.opacity = "0.5";
			var modalInstance = $uibModal.open({
			  animation: true,
			  templateUrl: 'caution.html',
			  controller: 'redownload-caution'
			});
	  }else{
		 navigator.notification.alert('No Event Found', function(){}, "Warning", "");
	  }
	};
	
});

aramcoApp.controller('redownload-caution', function ($scope, $uibModalInstance, $uibModal , $rootScope) {

		$scope.cancel = function () {
			$rootScope.resetbackground();
			$uibModalInstance.dismiss('cancel');
		};

		/* open device pin modal box */
		$scope.reset = function () {
			$uibModalInstance.dismiss('cancel');
			var modalInstance = $uibModal.open({
			  animation: true,
			  templateUrl: 'devicepin.html',
			  controller: 'redownload-devicepin'
			});
		};
 });

aramcoApp.controller('redownload-devicepin', function ($scope, $http, $uibModalInstance, $rootScope, EventService , $route ) {
	$scope.dataLoading = false;
	var candidates = null;
	$scope.application_pin = null;
	
	$scope.cancel = function () {
		$rootScope.resetbackground();
		$uibModalInstance.dismiss('cancel');
	};
	
	$scope.downloadevent = function(){
		if($rootScope.checkConnection()){
			$scope.reset()
		}
	}
	
	$scope.reset = function () {
		$scope.dataLoading = true;
		if($rootScope.$storage.pin != $scope.application_pin){
            navigator.notification.alert('Incorrect Application Pin.Please Try Again', function(){}, "Error", "");
			$scope.application_pin = null;
			$scope.dataLoading = false;
		}else{
			
			/* Get Event Data */
			EventService.getEventData($rootScope.$storage.current_event.event_id, function(response) {
				
				candidates = response.Candidates;
				var datetime = system_datetime();
				db.transaction(function (tx)
				{
					tx.executeSql('DELETE FROM candidates', []);
					for(index = 0;index < candidates.length ; index++)
					{
						var data = [candidates[index].CandidateId,candidates[index].CandidateInviteeCode,candidates[index].FirstName,candidates[index].LastName,candidates[index].Email,candidates[index].AreasOfInterestId,candidates[index].CurrentEmployerName,candidates[index].CurrentJobTitle,candidates[index].MobileNo,candidates[index].InviteeStatusId,candidates[index].JoinCommunityForEventFlage,candidates[index].ProfileSummary,0,1,datetime , null];
					
						tx.executeSql("INSERT INTO candidates(candidate_id, candidate_inviteecode, candidate_firstname, candidate_lastname,candidate_email,candidate_area_interest,candidate_employername,candidate_jobtitle,candidate_mobileno,candidate_invitee_statusid,candidate_joincommunity,candidate_profilesummary,candidate_dirty,candidate_synced,last_updated , error_message) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", 
							[
								data[0], data[1], data[2], data[3], data[4] ,data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]
							] ,
							function (tx, result) {
								console.log("Successfully Inserted : " + index);
							},
							function (tx, error) {
								console.log("Query Error: " + error.message + index);
							}
						); 
					}
				});
				
				$scope.dataLoading = false;
				$rootScope.resetbackground();
				$uibModalInstance.dismiss('cancel');
				navigator.notification.alert('Successfully Downloaded', function(){}, "Success", "");
                $route.reload();
			},
			function(){
				$scope.dataLoading = false;
				navigator.notification.alert('Something Went Wrong. Please Try again', function(){}, "Error", "");
			}
			
			);
		}
	};
	
});


