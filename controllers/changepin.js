aramcoApp.controller('changepin', function ($scope, $uibModalInstance,$rootScope) {
  $scope.error = false;
  $scope.formInfo = {};

  $scope.change = function () {
	
	if($rootScope.$storage.pin != $scope.formInfo.old_pin){
		$scope.error = 'Incorrect Old Pin';
	}else{
		if($scope.formInfo.new_pin != $scope.formInfo.confirm_new_pin){
			$scope.error = 'New Pin and Confirm New Pin must be same.';
		}else{
			$scope.error = false;
		}
	}
	  
	if(!$scope.error){
		$rootScope.$storage.pin = $scope.formInfo.new_pin;
        navigator.notification.alert('Successfully Changed', function(){}, "Success", "");
		$uibModalInstance.dismiss('cancel');
		$rootScope.resetbackground();
    }else {
        navigator.notification.alert($scope.error, function(){}, "Error", "");
    }
	  
  };	

  $scope.cancel = function () {
    $uibModalInstance.dismiss('cancel');
    $rootScope.resetbackground();
  };

  
});
